// Select your modem:
#define TINY_GSM_MODEM_SIM800

#include <TinyGsmClient.h>
#include <PubSubClient.h>
#include <LiquidCrystal_I2C.h> //Libreria LCD
#include "DHT.h"
#include <EEPROM.h>

 /******************************************************/
/* Variables de  ***************************************/
long analogPin0 = 0, analogPin1 = 1, val = 0, valtemp=0;
float  rsensor = 0, vr1, vsensor, R1;
int steps=0; 
long intervalo=1;
unsigned long current_millis;
#define minuto 5000
#define myled 5
#define myled2 6
#define resistencia 3400


#define DHTTYPE DHT11
#define DHTPIN 8
DHT dht(DHTPIN, DHTTYPE);



// Your GPRS credentials
// Leave empty, if missing user or pass
const char apn[]  = "em";
const char user[] = "";
const char pass[] = "";

// Use Hardware Serial on Mega, Leonardo, Micro
#define SerialAT Serial
TinyGsm modem(SerialAT);
TinyGsmClient client2(modem);
PubSubClient client(client2);

//Datos Mosquitto
const char* mqttServer = "tecnoandina-server.ddns.net";
//const char* mqttServer = "iot.eclipse.org";
const int mqttPort = 1883;
const char* mqttUser = "tecnoandina";
const char* mqttPassWd = "kennedy5600";
#define modelNumber  "TecAnd0000"
#define serialNumber "yulman"
const char* mqttID = serialNumber;

LiquidCrystal_I2C lcd(0x3f, 20, 4);

void setup() {
current_millis=5000;

EEPROM.write(1,1);
   dht.begin();
  pinMode(myled, OUTPUT);
  pinMode(myled2, OUTPUT);
  Serial.begin(115200);
  delay(10);

  // LCD
  lcd.init();
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(F("TecnoAndina SpA."));
  lcd.setCursor(0, 1);
  lcd.print(F("prueba"));
  lcd.setCursor(0, 2);
  lcd.print(F("Date :"));
  lcd.print(__DATE__);


  // Set GSM module baud rate
  SerialAT.begin(115200);
  delay(3000);

  // Restart takes quite some time
  // To skip it, call init() instead of restart()
  //Serial.println(F("Initializing modem..."));
   lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(F("Initializing modem"));
  modem.restart();

modem.getModemInfo();
  //Serial.print(F("Modem: "));
  //Serial.println(modemInfo);

  // Unlock your SIM card with a PIN
  //modem.simUnlock("1234");


  lcd.setCursor(0, 1);
  lcd.print(F("Waiting Network"));
  //Serial.print(F("Waiting for network..."));
  if (!modem.waitForNetwork()) {
  //  Serial.println(F(" fail"));
    while (true);
  }

   lcd.print(F(" OK"));
  Serial.println(F(" conexion a red OK"));

  Serial.print(F("Connecting to: "));
  delay(2000);
  //Serial.println(apn);
   lcd.setCursor(0, 2);
  lcd.print(F("Connecting APN"));
  if (!modem.gprsConnect(apn, user, pass)) {
    Serial.println(F(" fail"));
     lcd.setCursor(0, 2);
    lcd.print(F("                    "));
    lcd.setCursor(0, 2);
    lcd.print(F("Network Fail"));
    while (true);
  }

 lcd.setCursor(0, 2);
    lcd.print(F("                    "));
    lcd.setCursor(0, 2);
lcd.print(F("Connecting APN OK"));
  //Serial.println("conexion al APN OK");

mqtt_connect();

}






void loop() {



//************************************************TELEGRAPH**************************************************************
String telegraph="soilMoisture,modelNumber=pp0011,serialNumber=9YLQMR,verSoft=0.1 ";
//************************************************TELEGRAPH**************************************************************



if (!client.connected())
{
reconnect();
}
client.loop();



float h = dht.readHumidity();
float t = dht.readTemperature();



telegraph=telegraph+"soilMoist="+(String)humedad()+",dhtHum="+(String)h+",dhtTemp="+(String)t+",bat="+(String)medicion();
client.publish("pp0011/9YLQMR/output/status",telegraph.c_str());


delay(300000);
}



//********************************************CALLBACK********************************************************************

void callback(char* topic, byte* payload, unsigned int length) {

  const char len = length;
  char mensaje[len];
  String TOPic = topic;
 
if(TOPic=="pp0011/9YLQMR/input/intervalo")
 {

 
for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }
String payld=mensaje;
EEPROM.write(1,atoi(payld.c_str()));

 }

if(TOPic=="pp0011/9YLQMR/input/LCD")
{

for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }

 if(!(strncmp(mensaje,"on",2)))
  {
   
   lcd.backlight();

  }

  if(!(strncmp(mensaje,"off",3)))
  { 
  

  lcd.noBacklight();
  
  }

}



}

void mqtt_connect()
{
  //********************LOOP MQTT******************************
//***********************************************************
lcd.setCursor(0, 3);
    lcd.print(F("                    "));
    lcd.setCursor(0, 3);
    lcd.print(F("Connecting MQTT"));
    
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
  while (!client.connected()) {
    Serial.print(F("Connecting to MQTT "));
    Serial.println(mqttServer);

   if (client.connect(mqttID,mqttUser,mqttPassWd,"humedad/estudio",1,1,"OFFLINE")) { 
  client.publish("humedad/estudio","ONLINE",true);
   Serial.println(F("Yulman Connected to MQTT Broker"));
   Serial.println(F("----------------------------------------"));
      lcd.setCursor(0, 3);
      lcd.print(F("                    "));
      lcd.setCursor(0, 3);
      lcd.print(F("MQTT OK"));
    }//End If
    else {
      //Serial.print("failed with state: ");
      //Serial.println(client.state());
      delay(2000);
    }//End Else

client.subscribe("pp0011/9YLQMR/input/intervalo");

}//END While
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
   
   if (client.connect(mqttID,mqttUser,mqttPassWd,"humedad/estudio",1,1,"OFFLINE")) { 
//client.publish("status/gsm","ONLINE",true);
    client.publish("humedad/estudio","ONLINE",true);
client.subscribe("pp0011/9YLQMR/input/intervalo");

    } 
      delay(500);
    }
  }



 int humedad()
 {

   #define retardo 5
   String sensor="";
 
for(int i=0;i<50;i++)
{

 digitalWrite(myled, LOW);   
 digitalWrite(myled2, HIGH);
 val =val+analogRead(analogPin1);
 delay(retardo);      
  
  
 digitalWrite(myled2, LOW);
 digitalWrite(myled, HIGH);    
 val = val+analogRead(analogPin0);
 digitalWrite(myled, LOW);   
 delay(retardo);    

}

Serial.println();

valtemp=val/100;
vr1=((valtemp*4.3)/((4.3*1024)/5));
vsensor=4.3-vr1;
rsensor=(vsensor)/(vr1/3300);
R1=vr1/(vsensor/3300);


sensor=String(rsensor);

val=0;

return atoi(sensor.c_str());
 }





float medicion()
{
  
  
  steps++;
  int f=3;
  char _voltaje[10];
  float a = analogRead(3);
  float b = (a*12)/800; 



  String k= (String)b;

 

if(steps>120)
{
     steps=0;

}
return b;
}
